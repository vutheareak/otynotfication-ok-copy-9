import { NgModule } from '@angular/core';
import { YoutubePipe } from './youtube/youtube';
import { FbPipe } from './fb/fb';
@NgModule({
	declarations: [YoutubePipe,
    FbPipe],
	imports: [],
	exports: [YoutubePipe,
    FbPipe]
})
export class PipesModule {}
