import { Pipe, PipeTransform } from '@angular/core';

import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'fb',
})
export class FbPipe implements PipeTransform {
  
  constructor(private dom: DomSanitizer){
    
      }
    

  transform(value,args) {
    return this.dom.bypassSecurityTrustResourceUrl(value);
    // console.log(this.dom.bypassSecurityTrustResourceUrl("https://m.facebook.com/plugins/video.php?href="+value))
    
    // return this.dom.bypassSecurityTrustResourceUrl("https://m.facebook.com/plugins/video.php?href="+value);
    
  }
}
