import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';  
import { OneSignal } from '@ionic-native/onesignal'; // One Signal
import {CodePush} from '@ionic-native/code-push'; //Code Push
import { InAppBrowser } from '@ionic-native/in-app-browser';// In-app-browser

import {FbPipe} from '../pipes/fb/fb';
import  {YoutubePipe} from '../pipes/youtube/youtube';
import {BtvPage} from '../pages/btv/btv'; // Btv Page
import {InNewsPage} from '../pages/in-news/in-news'; //International News
import {NNewsPage} from '../pages/n-news/n-news'; //National News
import {SportNewsPage} from '../pages/sport-news/sport-news'; //SportNews
import {ENewsPage} from '../pages/e-news/e-news'; //Economic News
import {JobNewsPage} from '../pages/job-news/job-news'; //Job News
import {WatchnowPage} from '../pages/watchnow/watchnow'; //Watch Now
import {SettingPage} from '../pages/setting/setting';//setting Page
import {PlaylistListPage} from '../pages/playlist-list/playlist-list';//Play list Page
import {UpdatePage} from '../pages/update/update';//GO To Update Page
import {AudioStreamPage} from '../pages/audio-stream/audio-stream';//Page Audio 
import {ContactUsPage} from '../pages/contact-us/contact-us';//Page Contact us
import {RegisterPhonePage} from '../pages/register-phone/register-phone'; //page phone register
import {LoginPage} from '../pages/login/login';
import {LivePage} from '../pages/live/live';

import { RssProvider } from '../providers/rss/rss'; //Rss feed Provider
import { Network } from '@ionic-native/network';// For network detect
import { SocialSharing } from '@ionic-native/social-sharing';
import { YoutubeServiceProvider } from '../providers/youtube-service/youtube-service';//Social Sharing

import {IonicStorageModule} from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';

import { Facebook } from '@ionic-native/facebook';
import { ShrinkingSegmentHeaderComponent } from '../components/shrinking-segment-header/shrinking-segment-header';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { ContentDetailPage } from '../pages/content-detail/content-detail';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BtvPage,
    InNewsPage,
    NNewsPage,
    SportNewsPage,
    ENewsPage,
    JobNewsPage,
    WatchnowPage,
    YoutubePipe,
    FbPipe,
    SettingPage,
    PlaylistListPage,
    UpdatePage,
    ShrinkingSegmentHeaderComponent,
    AudioStreamPage,
    ContactUsPage,
    ContentDetailPage,
    RegisterPhonePage,
    LoginPage,
    LivePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BtvPage,
    InNewsPage,
    NNewsPage,
    SportNewsPage,
    ENewsPage,
    JobNewsPage,
    WatchnowPage,
    SettingPage,
    PlaylistListPage,
    UpdatePage,
    AudioStreamPage,
    ContactUsPage,
    ContentDetailPage,
    RegisterPhonePage,
    LoginPage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    OneSignal,
    CodePush,
    InAppBrowser,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RssProvider,
    SocialSharing,
    YoutubeServiceProvider,
    CallNumber,
    Facebook,
    StreamingMedia
  ]
})
export class AppModule {}
