import { Component } from '@angular/core';
import { Platform,ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { SplashPage } from '../pages/splash/splash';
import { HomePage } from '../pages/home/home';
import { OneSignal } from '@ionic-native/onesignal';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  rootPage:any = HomePage;

  constructor(platform: Platform, private statusBar: StatusBar, splashScreen: SplashScreen , private oneSignal: OneSignal, modalCtrl: ModalController) {
    platform.ready().then(() => {
      // this.statusBar.backgroundColorByHexString('#ffffff');
      
      if(this.statusBar) {
        // this.statusBar.overlaysWebView(false);
        // this.statusBar.backgroundColorByHexString('#222');
        this.statusBar.hide();
        
      }

      
      //splashScreen.show();
      setTimeout(function() {
        this.splashscreen.hide();
    }, 1);
      //OneSignal Code start:
       //Enable to debug issues:
       

       this.oneSignal.startInit('19666aa8-8314-4a03-b554-03b0301a8db9', '480237277903');
       
       this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
       
       this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when notification is received
       });
       
       this.oneSignal.handleNotificationOpened().subscribe(() => {
         // do something when a notification is opened
       });
       
       this.oneSignal.endInit();

       //
      /*window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
  
      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };
  
      window["plugins"].OneSignal
        .startInit("19666aa8-8314-4a03-b554-03b0301a8db9", "480237277903")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
        */
      
       
    });
  }
}
