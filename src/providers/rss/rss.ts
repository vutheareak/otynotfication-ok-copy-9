//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
/*
  Generated class for the RssProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RssProvider {

  description: string;
  link: string;
  title: string;

  constructor(public http: Http) {
    console.log('Hello RssProvider Provider');
  }
// GetRSS(){
//   const RSS_URL: any = 'https://news.google.com/news/rss/headlines/section/topic/WORLD?ned=us&hl=en&gl=US';
//   const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
//   const count: any= 10;
//   const API_URL: any= 'https://api.rss2json.com/v1/api.json';
//   const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json());
//   return response;
// }

GetRSS_Sport(){
  const RSS_URL: any = 'https://www.meandmyson.tk/feedsport';
  const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
  const count: any= 10;
  const API_URL: any= 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.meandmyson.tk%2Ffeedsport&api_key=o0lqwxbguzojktcfobh3adkqgyrpeh4sd4u2izfg';
  const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json());
  return response;
}

GetRSS_InNews(){
  const RSS_URL: any = 'https://www.meandmyson.tk/feedinternational';
  const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
  const count: any= 10;
  const API_URL: any= 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.meandmyson.tk%2Ffeedinternational&api_key=o0lqwxbguzojktcfobh3adkqgyrpeh4sd4u2izfg';
  const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json())

  return response;
 }
  

GetRSS_NNews(){
  const RSS_URL: any = 'https://www.meandmyson.tk/feednational';
  const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
  const count: any= 10;
  const API_URL: any= 'https://api.rss2json.com/v1/api.json?rss';
  //const response= this.http.post('https://query.yahooapis.com/v1/public/'+encodeURIComponent('https://www.meandmyson.tk/feednational')+'%22&format=json).map(res=>res.json();
  const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json().catch(error => { console.log(error) }));
  return response;
}

GetRSS_TopNews(){
  const RSS_URL: any = 'https://www.meandmyson.tk/feedtop';
  const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
  const count: any= 50;
  const API_URL: any= 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.meandmyson.tk%2Ffeedtop&api_key=o0lqwxbguzojktcfobh3adkqgyrpeh4sd4u2izfg';
  const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json());
  return response;
}

GetRSS_JobNews(){
  //Use Link Technology
  const RSS_URL: any = 'https://www.meandmyson.tk/feedtechnology';
  const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
  const count: any= 50;
  const API_URL: any= 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.meandmyson.tk%2Ffeedtechnology&api_key=o0lqwxbguzojktcfobh3adkqgyrpeh4sd4u2izfg';
  const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json());
  return response;
}

GetRSS_ENews(){
  //Economic News
  const RSS_URL: any = 'https://www.meandmyson.tk/feedeconomic';
  const API: any = 'ho9pnjmusikmivl9a9ks2zmwf1wktv38arlduerf';
  const count: any= 50;
  const API_URL: any= 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.meandmyson.tk%2Ffeedeconomic&api_key=o0lqwxbguzojktcfobh3adkqgyrpeh4sd4u2izfg';
  const response= this.http.post(API_URL, {'rss_url' : RSS_URL, 'api_key': API, 'count':count }).map(res=>res.json());
  return response;
}

GetRSS_topNew(){
  const feedUrl: any = 'https://www.meandmyson.tk/feedtop';
  var url = 'https://query.yahooapis.com/v1/public/yql?q=select%20title%2Clink%2Cdescription%20from%20rss%20where%20url%3D%22'+encodeURIComponent(feedUrl)+'%22&format=json';
  let articles = [];
  return this.http.get(url)
  .map(data => data.json()['query']['results'])
  .map((res) => {
    if (res == null) {
      return articles;
    }
    let objects = res['item'];
    var length = 20;

    
    return articles
  })
}
}
