import { Http, URLSearchParams, Response } from '@angular/http';
import { Injectable, NgZone } from '@angular/core';
import { window } from '@angular/platform-browser/src/facade/browser';

@Injectable()
export class YoutubeServiceProvider {
  youtube: any = {
    ready: false,
    player: null,
    playerId: null,
    videoId: null,
    videoTitle: null,
    playerHeight: '100%',
    playerWidth: '100%'
  }


  constructor (public http:Http) 
  {
    this.setupPlayer();
  }

  setupPlayer() 
  {
    window['onYouTubeIframeAPIReady'] = () => {
      if (window.YT) 
      {
        this.youtube.ready = true;
        this.bindPlayer('placeholder');
        this.youtube.player = this.createPlayer();
      }
    };
  }

  bindPlayer(elementId): void 
  {
    this.youtube.playerId = elementId;
  };

  createPlayer(): void 
  {
    return new window.YT.Player(this.youtube.playerId, {
      height: this.youtube.playerHeight,
      width: this.youtube.playerWidth,
      playerVars: {
        playsinline:1,
        rel: 0,
        showinfo: 1,
        allowfullscreen:1,
        webkitallowfullscreen:1,
      }
    });
  }

  loadPlayer(): void 
  {
    if (this.youtube.ready && this.youtube.playerId) 
    {
      if (this.youtube.player) 
      {
        this.youtube.player.destroy();
      }
      this.youtube.player = this.createPlayer();
    }
  }

  launchPlayer(id, title):void 
  {
    this.youtube.player.loadVideoById(id);
    this.youtube.videoId = id;
    this.youtube.videoTitle = title;
    return this.youtube;
  }
}
