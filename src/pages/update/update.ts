import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { CodePush ,SyncStatus} from '@ionic-native/code-push';


@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private codePush:CodePush ,public platform: Platform) {
  ////update release my-tes-android of android
  ////update release my-test of ios
    this.platform.ready().then(() => {
      this.codePush.sync( {}, (progress) =>{
         }).subscribe((status)=>{
          if(status == SyncStatus.CHECKING_FOR_UPDATE)
          alert("chcking For Update Now");
          if(status == SyncStatus.DOWNLOADING_PACKAGE)
          alert("Downloading Package");
          
          if(status == SyncStatus.IN_PROGRESS)
          alert("In progress");
          
          if(status == SyncStatus.UP_TO_DATE)
          //alert("Up to Date");
          this.Up_To_Date();
          if(status == SyncStatus.UPDATE_INSTALLED)
          //alert("Your App is Now Up to date!!! Please Click on Restart button to get back!!!");
          this.openUpdate1();
          //this.codePush.restartApplication();
          // this.openUpdate1();
          
          
          if(status == SyncStatus.ERROR)
          alert("ERROR");
            
          
            })
          })

  }
//update Code Push
 openUpdate1(){
   alert("Your App is Now Up to date!!! Please Click on Restart button to get back!!!")
  //window.location.reload();}
  this.Up_To_Date();
 }
//  //this.codePush.restartApplication();
// //this.codePush.getCurrentPackage();
// }
  
Up_To_Date(){
  alert("Your App is Up To Date")
 this.codePush.restartApplication().then(()=>{
 this.check_for_update();
})
//this.codePush.getCurrentPackage();
}


check_for_update(){

  this.platform.ready().then(() => {
    this.codePush.sync( {}, (progress) =>{
       }).subscribe((status)=>{
        if(status == SyncStatus.CHECKING_FOR_UPDATE)
        alert("chcking For Update Now");
                
        if(status == SyncStatus.IN_PROGRESS)
        alert("In progress");
        
        if(status == SyncStatus.UP_TO_DATE)
        //alert("Up to Date");
        this.Up_To_Date();
     
          
        
          })
        })

}
}
