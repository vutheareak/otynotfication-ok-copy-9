import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JobNewsPage } from './job-news';

@NgModule({
  declarations: [
    JobNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(JobNewsPage),
  ],
})
export class JobNewsPageModule {}
