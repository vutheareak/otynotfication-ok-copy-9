import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WatchnowPage } from './watchnow';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Platform } from 'ionic-angular';
@NgModule({
  declarations: [
    WatchnowPage,
  ],
  imports: [
    IonicPageModule.forChild(WatchnowPage),
  ],
})
export class WatchnowPageModule {
  constructor(platform: Platform, statusBar: StatusBar){

   
  }
}
