import { Component, ViewChild } from '@angular/core';
import { NavController, Navbar, Platform ,NavParams, ActionSheetController} from 'ionic-angular';
import {InNewsPage} from '../in-news/in-news'; //International News
import {NNewsPage} from '../n-news/n-news'; //National News
import {SportNewsPage} from '../sport-news/sport-news'; //SportNews
import {ENewsPage} from '../e-news/e-news'; //Economic News
import {JobNewsPage} from '../job-news/job-news'; //Job News
//import {WatchnowPage} from '../watchnow/watchnow'; // Watchnow page
import {SettingPage} from '../setting/setting'; // Watchnow page
import {HomePage} from '../home/home'; // Home page
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { PopoverController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Modal, Alert} from 'ionic-angular';
import { Slides } from 'ionic-angular';

import 'rxjs/add/operator/map';

// import {PlaylistListPage} from '../playlist-list/playlist-list';
import {YoutubeServiceProvider} from '../../providers/youtube-service/youtube-service';
/**
 * Generated class for the WatchnowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-watchnow',
  templateUrl: 'watchnow.html',
  providers:[YoutubeServiceProvider],
})
export class WatchnowPage {
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Slides) slides: Slides;
  
  // videos: any[]=[
  //   {
  //     title: 'How far i will go',
  //     video: 'https://www.youtube.com/embed/FbeaY3BHXVA',
  //   },
  //   {
  //     title: 'BTV news live',
  //     video: 'https:/www.youtube.com/embed/956Hq9DQdaI',
  //   }
  // ]

  
  channelID: string = 'UCupvZG-5ko_eiXAupbDfxWw';
  maxResults: string = '10';
  pageToken: string; 
  googleToken: string = 'AIzaSyB1Uh_Iwez-_wvF6SupCheHfu9LsW7Cq6w';
  searchQuery: string = '';
  posts: any = [];
  onPlaying: boolean = false; 
  data1:any = [];
  data2:any = [];
  data3:any = [];


  constructor(public navCtrl: NavController, public navParams: NavParams,public platform: Platform,
    public actionsheetCtrl: ActionSheetController,public http:Http,public ytPlayer: YoutubeServiceProvider,
    public popoverCtrl:PopoverController,private splashScreen: SplashScreen) {

    
      this.getdata3();
      this.getdata1();
      this.getdata2();
      this.getautoplay();
  
  }
  goToSlide() {
    this.slides.slideTo(2, 500);
  }
  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent)=>{
    window.location.reload();
    //  this.navCtrl.pop();
    }
  }

  getautoplay(){
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
  }
  loadautoplay():void{
    this.getautoplay();
  }

  launchYTPlayer(id, title): void {
    this.ytPlayer.launchPlayer(id, title);
    
 
  }



  getdata1(): void {
    
    let url='https://www.googleapis.com/youtube/v3/playlistItems?part=id,snippet&channelId='+
    'snippet&maxResults=50&playlistId=PL6XRrncXkMaUePR7EOfMNJoJ-1VIf0fRc&key='+ this.googleToken;
  
    
        if(this.pageToken) {
          url += '&pageToken=' + this.pageToken;
        }
    
        this.http.get(url).map(res => res.json()).subscribe(data => {
          
          console.log (data.items);
          
          this.data1 = this.data1.concat(data.items);
        });
      }

      getdata2(): void {
        
        let url='https://www.googleapis.com/youtube/v3/playlistItems?part=id,snippet&channelId='+
        'snippet&maxResults=50&playlistId=LLGLFlftDG26sjzrvtaOb2iQ&key='+ this.googleToken;
      
            if(this.pageToken) {
              url += '&pageToken=' + this.pageToken;
            }
            this.http.get(url).map(res => res.json()).subscribe(data => {
              
              console.log (data.items);
              
              this.data2 = this.data2.concat(data.items);
            });
          }

          getdata3(): void {
            
            let url='https://www.googleapis.com/youtube/v3/playlistItems?part=id,snippet&channelId='+
            'snippet&maxResults=50&playlistId=LLGLFlftDG26sjzrvtaOb2iQ&key='+ this.googleToken;
          
                if(this.pageToken) {
                  url += '&pageToken=' + this.pageToken;
                }
                this.http.get(url).map(res => res.json()).subscribe(data => {
                  
                  console.log (data.items);
                  
                  this.data3 = this.data3.concat(data.items);
                });
              }
      
      loaddata():void{
        this.getdata1();
      }
      loaddata2():void{
        this.getdata2();
      }
      loaddata3():void{
        this.getdata3();
      }


  openSettings(): void {
      console.log("TODO: Implement openSettings()");
  }
  playVideo(e, post): void {
      console.log(post);
      this.onPlaying = true;
      this.ytPlayer.launchPlayer(post.snippet.resourceId, post.snippet.title);
  }
  loadMore(): void {
      console.log("TODO: Implement loadMore()");
  }





//go Default home page
  onclickHome()
  {
    // window.location.reload();
    
    // this.navCtrl.push(HomePage);
  }


  
}
