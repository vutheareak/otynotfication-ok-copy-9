import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ENewsPage } from './e-news';

@NgModule({
  declarations: [
    ENewsPage,
  ],
  imports: [
    IonicPageModule.forChild(ENewsPage),
  ],
})
export class ENewsPageModule {}
