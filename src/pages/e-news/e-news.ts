import { Component } from '@angular/core';
import { NavController , IonicPage,Platform , ActionSheetController} from 'ionic-angular';

import {InNewsPage} from '../in-news/in-news'; //International News
import {NNewsPage} from '../n-news/n-news'; //National News
import {SportNewsPage} from '../sport-news/sport-news'; //SportNews
import {HomePage} from '../home/home'; //Economic News
import {JobNewsPage} from '../job-news/job-news'; //Job News
import {WatchnowPage} from '../watchnow/watchnow'; // Watchnow page
import {SettingPage} from '../setting/setting'; // Watchnow page

import { Http, Headers, RequestOptions } from '@angular/http';//Http Request
//inapp browser
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';
//import RssFeed Provider
import { RssProvider } from '../../providers/rss/rss';


@IonicPage()
@Component({
  selector: 'page-e-news',
  templateUrl: 'e-news.html',
})
export class ENewsPage {

  //rssDataArray: any = [];//rss feed data array
  public contents: any = [];
  public link: string;
    constructor(public navCtrl: NavController, public platform: Platform,
      public actionsheetCtrl: ActionSheetController,public rssProvider: RssProvider,
      private theInAppBrowser: InAppBrowser,private http:Http) {
  
        //get_rss_feed_data
   //this.Get_RSS_Data()
   this.getItem()
    }
     
    getItem() {
      this.http.get('https://www.meandmyson.tk/feedeconomic')
        .map(res => res.json())
        .subscribe(
          data => {
            let re = data.items;
            this.contents = re;
            this.link = data.feed.link
          },
          err => {
            alert(err);
          }
          
        );
      }
      doRefresh(refresher) {
        //console.log('Begin async operation', refresher);
        console.log(this.getItem(),refresher);
        setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }
    


    ////---------Request RSS Feed-------------/////
   
    //  Get_RSS_Data(){
    //    this.rssProvider.GetRSS_ENews().subscribe(
    //      data =>{
    //        this.rssDataArray = data;
    //        console.log(data);
    //      }
    //    );
    //  }


  // slide menu up click HomePage
onclickHome(){
  this.navCtrl.setRoot(HomePage,{
    val: 'Clicked on to National News Page'
  })
}
//click go to watch now
  onclickWatchnow(){
    this.navCtrl.setRoot(WatchnowPage,{
      val: 'go to watchnow youtube'
    })
  }
  //
  //Click to go to setting page
  onclickSetting(){
    this.navCtrl.setRoot(SettingPage,{
      val: 'go to Setting page'
    })
  }
  
  
  // slide menu up click National News
    onclickNNews(){
      this.navCtrl.setRoot(NNewsPage,{
        val: 'Clicked on to National News Page'
      })
    }
  
    // slide menu up click International News
    onclickINNews(){
      this.navCtrl.setRoot(InNewsPage,{
        val: 'Clicked on to International News Page'
      })
    }
  
    // slide menu up click Sport News
    onclickSportNews(){
      this.navCtrl.setRoot(SportNewsPage,{
        val: 'Clicked on to Economic News page'
      })
    }
  
    // slide menu up click Economic News
    onclickENews(){
      this.navCtrl.setRoot(ENewsPage,{
        val: 'Clicked on to Sport News Page'
      })
    }
  
    // slide menu up click Jobs News
    onclickJobNews(){
      this.navCtrl.setRoot(JobNewsPage,{
        val: 'Clicked on to Job New Page'
      })
    }
  
  //----------------------------------------------//
  
  
  
    
    
  //------------------------------------------------------//
  //Bottom Menu
  
    openMenu() {
      let actionSheet = this.actionsheetCtrl.create({
        title: 'មាតិការព័ត៌មាន',
        cssClass: 'action-sheets-basic-page',
        buttons: [
          {
            //Economic News Menu
          text: 'ព័ត៌មានថ្មីៗ',
          icon: !this.platform.is('ios') ? 'ios-globe' : null,
          handler: () => {
            //console.log('Favorite clicked');
            this.onclickHome()
          }
        },
          {
            //National News Menu
            text: 'ព័ត៌មានជាតិ',
            //role: 'destructive',
            icon: !this.platform.is('ios') ? 'paper' : null,
            
            
            handler: () => {
              //window.location.replace("/#/BtvPage");
              //console.log('Share clicked');
              this.onclickNNews()
            }
            
          },
          {
            //Internationa News Menu
            text: 'ព័ត៌មានអន្តរជាតិ',
            icon: !this.platform.is('ios') ? 'md-globe' : null,
            handler: () => {
              //console.log('Share clicked');
              this.onclickINNews()
            }
          },
          {
            //Sport News Munu
            text: 'ព័ត៌មានកីឡា',
            icon: !this.platform.is('ios') ? 'md-football' : null,
            handler: () => {
              //console.log('Play clicked');
              this.onclickSportNews()
            }
          },
          
          {
            //Jobs News Menu
            text: 'ព័ត៌មានការងារ',
            icon: !this.platform.is('ios') ? 'ios-people' : null,
            handler: () => {
              //console.log('Favorite clicked');
              this.onclickJobNews()
            }
          },
          {
            text: 'ចាកចេញ',
            role: 'cancel', // will always sort to be on the bottom
            icon: !this.platform.is('ios') ? 'close' : null,
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    }

  //-----------------In App Browser----------------------------------//

  options : InAppBrowserOptions = {
    location : 'no',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };
  
  
  
  public openWithInAppBrowser(url : string){
    let target = "_blank";
    this.theInAppBrowser.create(url,target,this.options);
  }

  }