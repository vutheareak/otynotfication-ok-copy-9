import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InNewsPage } from './in-news';

@NgModule({
  declarations: [
    InNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(InNewsPage),
  ],
})
export class InNewsPageModule {}
