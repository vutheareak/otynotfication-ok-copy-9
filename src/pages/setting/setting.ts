import { Component } from '@angular/core';
import { IonicPage, AlertController,NavController, NavParams , Platform , ActionSheetController} from 'ionic-angular';
import { CodePush ,SyncStatus} from '@ionic-native/code-push';
import {HomePage} from '../home/home';
import {UpdatePage} from '../update/update';
import {Http,RequestOptions,Headers} from '@angular/http';
import {InNewsPage} from '../in-news/in-news'; //International News
import {NNewsPage} from '../n-news/n-news'; //National News
import {SportNewsPage} from '../sport-news/sport-news'; //SportNews
import {ENewsPage} from '../e-news/e-news'; //Economic News
import {JobNewsPage} from '../job-news/job-news'; //Job News
import {WatchnowPage} from '../watchnow/watchnow'; // Watchnow page
import {PlaylistListPage} from '../playlist-list/playlist-list';//Go Playlist Page
import {ContactUsPage} from '../contact-us/contact-us';// Gon Contact us page

//import {SettingPage} from '../setting/setting'; // Watchnow page
import {Storage} from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';

import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  newstypePanel: boolean;
  testcheckboxResult: any;
  public contents: any;
  public link: string;
  private nts: any;
  constructor(public navCtrl: NavController,public alertCtrl: AlertController, public navParams: NavParams, 
    private codePush:CodePush ,public platform: Platform,private http:Http, private storage: Storage, private onesignal: OneSignal,
    private callNumber: CallNumber) 
  {
    
  }

  onclickUpdate(){
    this.updateMenu();
    this.navCtrl.setRoot(UpdatePage,{
      val: 'Clicked on to Update News Page'
    })
  }
  onclickHome(){
    this.navCtrl.setRoot(HomePage,{
      val: 'Clicked on to homePage News Page'
    })
  }
  selectNewsType()
  {
    this.storage.get('menu').then((menus) => {
      if(menus == null)
      {
        this.updateMenu();
        setTimeout(() => {
          this.storage.get('menu').then((menus) => {
            this.getNewsType(menus);
          });          
        }, 3000);
      }
      else
      {
        this.getNewsType(menus);
      }
    });
  }
  getNewsType(menus)
  {
    this.storage.get('newstype').then((values) => {
      let alert = this.alertCtrl.create();
      alert.setTitle('Select News Page');
      var a: any;
      for(let nt of menus)
      {
        var ch = false;
        var newstypes = [];
        newstypes['id'] = nt['id'];
        newstypes['name'] = nt['news_type_name'].toLowerCase();
        a = newstypes;
        for(let val of values['id'])
        {
          if(nt['id'] == val)
          {
            alert.addInput({
              type: 'checkbox',
              label: nt['kh_news_type_name'],
              value: a,
              checked: true
            });
            ch = true;
            break;
          }
        }
        if(ch == false)
        {
          alert.addInput({
            type: 'checkbox',
            label: nt['kh_news_type_name'],
            value: a,
          });
        }
      }
      alert.addButton('Cancel');
      alert.addButton({
        text: 'Ok',
        handler: items => {
          for(let menu of menus)
          {
            this.onesignal.deleteTag(menu['news_type_name'].toLowerCase());
          }
          var newstypes = [];
          let arr = [];
          let arr1 = [];
          for(let item of items)
          {
            arr.push(item.id);
            arr1.push(item.name);
            this.onesignal.sendTag(item.name, item.id);
          }
          newstypes['id'] = arr;
          newstypes['name'] = arr1
          this.storage.set('newstype', newstypes);
          var headers = new Headers();
          headers.append('Access-Control-Allow-Origin', '*');
          headers.append("Accept", 'application/json');
          headers.append('Content-Type', 'application/json' );
          let options = new RequestOptions({ headers: headers });
          let postParams = {
            id: newstypes['id']
          }
          this.http.post("https://www.meandmyson.tk/feed", postParams, options)
            .subscribe(data => {
              let res = JSON.parse(data['_body']);
              this.contents = res;
              this.navCtrl.setRoot(HomePage, this.contents);
             }, error => {
              console.log(error);
            });
        }
      });
      alert.present().then ( () => {
        this.newstypePanel = true;
      });
    });
  }
  updateMenu()
  {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    this.http.get("https://www.meandmyson.tk/feed/news_types", options)
      .subscribe(data => {
        let res = JSON.parse(data['_body']);
        this.storage.set('menu', res);
      }, error => {
        console.log(error);
      });
  }

 

  playlistPage()
  {
    this.navCtrl.push(PlaylistListPage,'go to playlist');
  }
  calladvertisement()
  {
    this.callNumber.callNumber("0966844776", true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
  }
  onclickwatchnow()
  {
    this.navCtrl.setRoot(WatchnowPage,'on click watchnow page');
  }
  openfb()
  {
    window.open('fb://profile/369492713501851', '_system', 'location=no');
  }
  gocontactus()
  {
    this.navCtrl.push(ContactUsPage);
  }
  
}
