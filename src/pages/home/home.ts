import { Component, ViewChild } from '@angular/core';
import { NavController ,NavParams,Slides ,Platform , ActionSheetController, ToastController} from 'ionic-angular';
//import { CodePush ,SyncStatus} from '@ionic-native/code-push';
//import { BtvPage } from '../btv/btv';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';


import {InNewsPage} from '../in-news/in-news'; //International News
import {NNewsPage} from '../n-news/n-news'; //National News
import {SportNewsPage} from '../sport-news/sport-news'; //SportNews
import {ENewsPage} from '../e-news/e-news'; //Economic News
import {JobNewsPage} from '../job-news/job-news'; //Job News
import {WatchnowPage} from '../watchnow/watchnow'; // Watchnow page
import {SettingPage} from '../setting/setting'; // Watchnow page
import {ContentDetailPage} from '../content-detail/content-detail';//Content Detail page
import {AudioStreamPage} from '../audio-stream/audio-stream'; //live audio page
import {RegisterPhonePage} from '../register-phone/register-phone';// register Phone Page
import {LoginPage} from '../login/login';
import {LivePage} from '../live/live';

import { RssProvider } from '../../providers/rss/rss';
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { App } from 'ionic-angular';
import { Network } from '@ionic-native/network';// Toast Network test the internet connection
import { SocialSharing } from '@ionic-native/social-sharing';// Social Sharing to Multi Application
import {Storage} from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';
import { Content } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // @ViewChild(Content) scroller: Content;
  // @ViewChild('imgSlides') imgSlides: Slides;
  @ViewChild(Content) content: Content;
  test;
  userData: any;
  public contents: any = [];
  public link: any = [];
  private singal: boolean = false;
  private data: any = [];
  private website: any = [
    'https://www.meandmyson.tk',
    'https://www.eemedia.tk',
    'https://www.ggmedia.tk',
   
  ];
  private shrink: boolean = false;
  private content_height: any;
  constructor(public navCtrl: NavController, public platform: Platform,
    public actionsheetCtrl: ActionSheetController, public appCtrl: App, private theInAppBrowser: InAppBrowser,
    public http: Http,public rssProvider: RssProvider,private toast: ToastController, private network: Network,
    private socialSharing: SocialSharing, public navparam:NavParams, private storage:Storage, private onesignal:OneSignal,
    private facebook: Facebook) 
  {
    
    // this.storage.remove('menu');
    // this.storage.remove('newstype');
    // this.storage.remove('single_id');
    // this.storage.remove('data');
    // console.log(this.storage.keys());
    this.storeData();
    this.singal = false;
    this.storage.get('newstype').then((val) => {
      if(val == null)
      {
        this.setNewsTypes();
        setTimeout(() => {
          this.storage.get('newstype').then((val) => {
            this.getContents(val['id']);
          });
        }, 3000);
      }
      else
      {
        this.storage.get('newstype').then((val)=>{
          if(Object.keys(val).length == 0)
          {
            this.setNewsTypes();
            setTimeout(() => {
              this.storage.get('newstype').then((val) => {
                this.getContents(val['id']);
              });
            }, 3000);
          }
          else
          {
            setTimeout(() => {
              this.getContents(val['id']);
            }, 3000);
          }
        });
      }
    });
  }
//   ionViewWillEnter(){
 
//     this.content.resize();
 
// }
  scroll(evt)
  {
    var s_top = evt.scrollTop;
    var header = document.getElementById('header_nav');
    var content = document.getElementById('content');
    if(evt.directionY == 'down' && !this.shrink)
    {
      header.className = 'fade_out';
      content.style.height = (content.offsetHeight + header.offsetHeight) + 'px';
      content.style.top = '-70px';
      this.shrink = true;
    }
    if(evt.directionY == 'up' && this.shrink)
    {
      header.className = 'fade_in';
      content.style.height = (content.offsetHeight - header.offsetHeight) + 'px';
      content.style.top = '0px';
      this.shrink = false;
    }
  }

  scroller() {
    this.content.ionScroll.subscribe((data)=>{
      this.content.resize();
    });
  }

  setNewsTypes()
  {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    this.http.get("https://www.meandmyson.tk/feed/news_types", options)
      .subscribe(data => {
        let res = JSON.parse(data['_body']);
        var newstypes = [];
        let arr = [];
        let arr1 = [];
        for(let i of res)
        {
          arr.push(i.id);
          arr1.push((''+i.news_type_name).toLowerCase());
          this.onesignal.sendTag((''+i.news_type_name).toLowerCase(), i.id);
        }
        newstypes['id'] = arr;
        newstypes['name'] = arr1;
        this.storage.set('newstype', newstypes);
      });
  }

  doRefresh(refresher) 
  {
    setTimeout(() => {
      this.storage.get('single_id').then((val)=>{
        this.storeData();
        setTimeout(() => {
          if(val == null)
          {
            this.storage.get('newstype').then((val)=>{
              this.getContents(val['id']);
              this.singal = false;
            });
          }
          else
          {
            this.getContents([val]);
          }  
        }, 3000);
      });
      refresher.complete();
    }, 2000);
  }

  getNewContents(evt)
  {
    if(this.singal)
    {
      this.singal = false;
      this.storage.get('newstype').then((val)=>{
        this.getContents(val['id']);
      });
    }
    else
    {
      this.content.scrollToTop();
     
    }
  }
  storeData()
  {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
 
    // this.http.post("https://www.meandmyson.tk/feed", postParams, options).subscribe(
    //   data_b => {
    //     let res_b = JSON.parse(data_b['_body']);
    //     this.data[1] = res_b.feed;
    //   },
    //   error => {
    //     console.log(error);
    //   });
    // this.http.post("https://www.eemedia.tk/feed", postParams, options).subscribe(
    //   data_a => {
    //     let res_a = JSON.parse(data_a['_body']);
    //     this.data[0] = res_a.feed;
    //    }, 
    //   error => {
    //     console.log(error);
    //   });
    this.data = [];
    for(let web of this.website)
    {
      this.http.get(web + "/feed", options).subscribe(
        data_a => {
          let res = JSON.parse(data_a['_body']);
          this.data.push(res.feed);
         }, 
        error => {
          console.log(error);
        });
    }
    setTimeout(()=>{
      var contents = [];
      for(let datas of this.data)
      {
        if(!!datas)
        {
          for(let data of datas)
          {
            contents.push(data);
          }
        }
      }
      this.storage.set('data', contents);
    }, 3000);
  }
  getContents(ids)
  {
    this.storage.get('data').then((contents) => {
      var arr = [];
      for(var con of contents)
      {
        for(var id of ids)
        {
          if(con.news_type_id == id)
          {
            arr.push(con);
          }
        }
      }
      this.sortDesc(arr);
      this.contents = arr;
    });
    if(!this.singal)
    {
      this.storage.remove('single_id');
    }
  }
  
  sortDesc(datas)
  {
    for(var i=0; i<datas.length; i++)
    {
      var tmp = new Date((datas[i].created_at).replace(/-/g,"/"));
      for(var j=i+1; j<datas.length; j++)
      {
        var tmp1 = new Date((datas[j].created_at).replace(/-/g,"/"));
        if(tmp1 > tmp)
        {
          tmp = tmp1;
          var a = datas[j];
          datas[j] = datas[i];
          datas[i] = a;
        }
      }
    }
  }

  //go Default home page
  onclickHome(){
    this.navCtrl.setRoot(HomePage,{
      val: 'Clicked on to National News Page'
    })
  }
  //wathcnow click to go youtube page
  onclickWatchnow(){
    
    this.navCtrl.push(WatchnowPage);
  }
  //Click to go to setting page
  onclickSetting(){
    this.navCtrl.push(SettingPage, 'Setting');
    var footer = document.getElementById('footer_nav');
    footer.style.bottom = '0px';
    document.body.style.bottom = '0px';
  }
  //Bottom Menu
  updateMenu()
  {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    this.http.get("https://www.meandmyson.tk/feed/news_types", options)
      .subscribe(data => {
        let res = JSON.parse(data['_body']);
        this.storage.set('menu', res);
      }, error => {
        console.log(error);
      });
  }
  getMenu()
  {
    var arr = [];
    this.storage.get('menu').then((res) => {
      for(let r of res)
      {
        arr.push({
          text: r.kh_news_type_name,
          icon: !this.platform.is('ios') ? ''+r.icon : null,
          handler: () => {
            this.getContents([r.id]);
            this.storage.set('single_id', r.id);
            this.singal = true;
          }
        });
      }
      let actionSheet = this.actionsheetCtrl.create({
        title: 'មាតិការពត៍មាន',
        cssClass: 'action-sheets-basic-page',
        buttons: arr
      });
      actionSheet.present();
    });
  }
  openMenu()
  {
    this.storage.get('menu').then((menus) => {
      if(menus == null)
      {
        this.updateMenu();
        setTimeout(() => {
          this.getMenu();
        }, 3000);
      }
      else
      {
        this.getMenu();
      }
    });
  }

//----------------- Start In App Browser----------------------------------//
options : InAppBrowserOptions = {
  location : 'no',//Or 'no' 
  hidden : 'no', //Or  'yes'
  clearcache : 'yes',
  clearsessioncache : 'yes',
  zoom : 'yes',//Android only ,shows browser zoom controls 
  hardwareback : 'yes',
  mediaPlaybackRequiresUserAction : 'no',
  shouldPauseOnSuspend : 'no', //Android only 
  closebuttoncaption : 'Close', //iOS only
  disallowoverscroll : 'no', //iOS only 
  toolbar : 'yes', //iOS only 
  enableViewportScale : 'no', //iOS only 
  allowInlineMediaPlayback : 'no',//iOS only 
  presentationstyle : 'pagesheet',//iOS only 
  fullscreen : 'yes',//Windows only    
};



public openWithInAppBrowser(url : string){
  let target = "_blank";
  this.theInAppBrowser.create(url,target,this.options);
}
//----------------- End In App Browser ----------------------------------//


//----------------- Start Toast Network detect Check ------------//
//Network Detection check online or not

displayNetworkUpdate(connectionState: string){
  let networkType = this.network.type;
  this.toast.create({
    message: `You are now ${connectionState}`,
    duration: 3000
  }).present();
}


ionViewDidEnter() {
  this.network.onConnect().subscribe(data => {
    console.log(data)
    this.displayNetworkUpdate(data.type);
  }, error => console.error(error));
 
  this.network.onDisconnect().subscribe(data => {
    console.log(data)
    this.displayNetworkUpdate(data.type);
  }, error => console.error(error));
}

//----------------- End Toast Network detect Check ------------//


//--------------------Start Date Share Data to Social App -----------------------//

  shareInfo(msg, subject, img, url)
  {
    this.socialSharing.share(msg, subject, img, url);
  }

  openfb(){
    window.open('fb://profile/369492713501851', '_system', 'location=no');
  }

  detail(title, detail, created_at)
  {
    var data = {
      title: title,
      detail: detail,
      date: created_at
    }
    this.navCtrl.push(ContentDetailPage, data);
  }
  // ionViewDidLoad(){
  //   setInterval(()=>
  // {
  //   if(this.imgSlides.getActiveIndex() == this.imgSlides.length() -1)
  //   this.imgSlides.slideTo(0);
  //   this.imgSlides.slideNext();
  // },3000)
  // }
  liveaudio(){
    this.navCtrl.push(AudioStreamPage);
  }
  registerphone(){
    this.navCtrl.push(RegisterPhonePage);
  }
  login(){
    this.navCtrl.push(LoginPage);
  }
  live(){
    this.navCtrl.push(LivePage);
  }
}
  

