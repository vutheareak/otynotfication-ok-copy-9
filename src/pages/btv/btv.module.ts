import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BtvPage } from './btv';

@NgModule({
  declarations: [
    BtvPage,
  ],
  imports: [
    IonicPageModule.forChild(BtvPage),
  ],
})
export class BtvPageModule {}
