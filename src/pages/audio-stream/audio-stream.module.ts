import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudioStreamPage } from './audio-stream';

@NgModule({
  declarations: [
    AudioStreamPage,
  ],
  imports: [
    IonicPageModule.forChild(AudioStreamPage),
  ],
})
export class AudioStreamPageModule {}
