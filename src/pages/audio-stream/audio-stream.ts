
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StreamingMedia, StreamingVideoOptions, StreamingAudioOptions } from '@ionic-native/streaming-media';

@IonicPage()
@Component({
  selector: 'page-audio-stream',
  templateUrl: 'audio-stream.html',
})
export class AudioStreamPage {

  constructor(private streamingMedia: StreamingMedia) { }
  

  
   startAudio() {
     let options: StreamingAudioOptions = {
       successCallback: () => { console.log('Finished Audio') },
       errorCallback: (e) => { console.log('Error: ', e) },
       initFullscreen: true, // iOS only!
       bgImage:"https://www.ffmedia.tk/radio.jpg",
     };
  
     
     this.streamingMedia.playAudio('https://stream.radio.co/sf957fb3ce/listen', options);
   }
  

  
 }
