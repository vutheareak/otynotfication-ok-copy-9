import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the PlaylistListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playlist-list',
  templateUrl: 'playlist-list.html',
})
export class PlaylistListPage {

  playlistid='LL18NaGQLruOEw65eQE3bNbg';
  maxRes:string='5';
  //googleToken:string='AIzaSyB1Uh_Iwez-_wvF6SupCheHfu9LsW7Cq6w';
  search:string='ionic';
  posts:any=[];
  //pageToken: string; 
  channelID: string = 'UCupvZG-5ko_eiXAupbDfxWw';
  maxResults: string = '10';
  pageToken: string; 
  googleToken: string = 'AIzaSyB1Uh_Iwez-_wvF6SupCheHfu9LsW7Cq6w';
  searchQuery: string = '';
  //posts: any = [];
  onPlaying: boolean = false; 



  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http) {

    let url='https://www.googleapis.com/youtube/v3/playlistItems?part=id,snippet&channelId='+
    'snippet&maxResults=50&playlistId=PLpaPR7OMlko7mF28xtQjKfGvrdYXq_0iH&key='+ this.googleToken;
  
    
        if(this.pageToken) {
          url += '&pageToken=' + this.pageToken;
        }
    
        this.http.get(url).map(res => res.json()).subscribe(data => {
          
          console.log (data.items);
          
          this.posts = this.posts.concat(data.items);
        });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaylistListPage');
  }

}
