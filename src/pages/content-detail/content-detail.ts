import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from '../home/home';


@IonicPage()
@Component({
  selector: 'page-content-detail',
  templateUrl: 'content-detail.html',
})
export class ContentDetailPage {

  private content: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) 
  {
    this.content = this.navParams.data;
    console.log(this.content)
  } 

clickhome(){
  this.navCtrl.setRoot(HomePage);
}
}
