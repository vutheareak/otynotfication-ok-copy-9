import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NNewsPage } from './n-news';

@NgModule({
  declarations: [
    NNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(NNewsPage),
  ],
})
export class NNewsPageModule {}
