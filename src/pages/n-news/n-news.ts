import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController,Platform } from 'ionic-angular';

import {HomePage} from '../home/home'; //National News
import {InNewsPage} from '../in-news/in-news'; //International News
import {SportNewsPage} from '../sport-news/sport-news'; //SportNews
import {ENewsPage} from '../e-news/e-news'; //Economic News
import {JobNewsPage} from '../job-news/job-news'; //Job News

import { Http, Headers, RequestOptions } from '@angular/http';//Http Request
//inapp browser
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';
//import RssFeed Provider
import { RssProvider } from '../../providers/rss/rss';
@IonicPage()
@Component({
  selector: 'page-n-news',
  templateUrl: 'n-news.html',
})
export class NNewsPage {

  //rssDataArray: any = [];//rss feed data array
  public contents: any = [];
  public link: string;
  
    constructor(public navCtrl: NavController, public platform: Platform,
      public actionsheetCtrl: ActionSheetController,public rssProvider: RssProvider,
      private theInAppBrowser: InAppBrowser,private http:Http) {
  
        //get_rss_feed_data
  // this.Get_RSS_Data()
   this.getItem()
    }
     
    ////---------Request RSS Feed National News-------------/////
   
    //  Get_RSS_Data(){
    //    this.rssProvider.GetRSS_NNews().subscribe(
    //      data =>{
    //        this.rssDataArray = data;
    //        console.log(data);
    //      }
    //    );
    //  }


    getItem() {
      this.http.get('https://www.meandmyson.tk/feednational')
        .map(res => res.json())
        .subscribe(
          data => {
            let re = data.items;
            this.contents = re;
            this.link = data.feed.link
          },
          err => {
            alert(err);
          }
          
        );
      }
      doRefresh(refresher) {
        //console.log('Begin async operation', refresher);
        console.log(this.getItem(),refresher);
        setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }



// slide menu up click HomePage
onclickHome(){

  this.navCtrl.setRoot(HomePage,{
    val: 'Clicked on to National News Page'
  })
}

// slide menu up click International News
onclickINNews(){
  this.navCtrl.setRoot(InNewsPage,{
    val: 'Clicked on to International News Page'
  })
}

// slide menu up click Sport News
onclickSportNews(){
  this.navCtrl.setRoot(SportNewsPage,{
    val: 'Clicked on to Economic News page'
  })
}

// slide menu up click Economic News
onclickENews(){
  this.navCtrl.setRoot(ENewsPage,{
    val: 'Clicked on to Sport News Page'
  })
}

// slide menu up click Jobs News
onclickJobNews(){
  this.navCtrl.setRoot(JobNewsPage,{
    val: 'Clicked on to Job New Page'
  })
}


  openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'មាតិការព័ត៌មាន',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          //National News Menu
          text: 'ព័ត៌មានថ្មីៗ',
          //role: 'destructive',
          icon: !this.platform.is('ios') ? 'paper' : null,
         
          handler: () => {
            //window.location.replace("/#/BtvPage");
            //console.log('Share clicked');
            this.onclickHome()
          }
          
        },
        {
          //Internationa News Menu
          text: 'ព័ត៌មានអន្តរជាតិ',
          icon: !this.platform.is('ios') ? 'md-globe' : null,
          handler: () => {
            //console.log('Share clicked');
            this.onclickINNews()
          }
        },
        {
          //Sport News Munu
          text: 'ព័ត៌មានកីឡា',
          icon: !this.platform.is('ios') ? 'md-football' : null,
          handler: () => {
            //console.log('Play clicked');
            this.onclickSportNews()
          }
        },
        {
            //Economic News Menu
          text: 'ព័ត៌មានសេដ្ឋកិច្ច',
          icon: !this.platform.is('ios') ? 'md-trending-up' : null,
          handler: () => {
            //console.log('Favorite clicked');
            this.onclickENews()
          }
        },
        {
          //Jobs News Menu
          text: 'ព័ត៌មានការងារ',
          icon: !this.platform.is('ios') ? 'ios-people' : null,
          handler: () => {
            //console.log('Favorite clicked');
            this.onclickJobNews()
          }
        },
        {
          text: 'ចាកចេញ',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  //-----------------In App Browser----------------------------------//

  options : InAppBrowserOptions = {
    location : 'no',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };
  
  
  
  public openWithInAppBrowser(url : string){
    let target = "_blank";
    this.theInAppBrowser.create(url,target,this.options);
  }

}
